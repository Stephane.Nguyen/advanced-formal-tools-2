# Modified theorems from Natural Number Game

- https://adam.math.hhu.de/#/g/leanprover-community/nng4

They are modified because `induction with d hd` do not exist and `rfl` tactic in Lean is stronger than the one in the game.
