
-- Define the inductive type MyNat (it's a sum type)
inductive MyNat where
  | zero : MyNat
  | succ (n : MyNat) : MyNat

-----------------------------------------------------
-- To make it easier to write (cannot use 'def' keyword)
abbrev zero : MyNat := MyNat.zero
abbrev succ : MyNat -> MyNat := MyNat.succ

-- Definition of the function addition
def MyNat.plus (a : MyNat) (b : MyNat) : MyNat :=
  match b with
  | zero => a
  | succ b' => succ (MyNat.plus a b')

-- overloading the "+"
instance : Add MyNat where
  add := MyNat.plus

-- Axioms of addition (they are functions)
-- n + 0 = n is a type in itself
theorem add_zero: n + zero = n :=
  rfl -- strong
theorem add_succ: a + succ b = succ (a + b) :=
  rfl -- strong
-- TODO: make rfl less strong?

-----------------------------------------------------
-- Some modified proofs from the game
-- theorem succ_eq_add_one n : succ n = n + succ zero := by
--   rw [add_succ, add_zero]
--   -- rfl included..

theorem zero_add n : zero + n = n := by
  -- It looks different than the game where
  -- we used induction n with d hd
  induction n with
  | zero => rw [add_zero]
  | succ d hd => rw [add_succ, hd]

theorem succ_add a b : (succ a) + b = succ (a + b) := by
  induction b with
  | zero => rw [add_zero, add_zero]
  | succ d hd => rw [add_succ, add_succ, hd]

--------------------------------------------------------
-- Theorem add_comm: On the set of natural numbers, addition is commutative.
-- In other words, if a and b are arbitrary natural numbers, then a+b=b+a.
theorem add_comm (a: MyNat) (b: MyNat): a + b = b + a := by
  induction a with
  | zero => rw [zero_add, add_zero]
  | succ d hd => rw [succ_add, add_succ, hd]

-- Random example
-- theorem bla : n + zero + zero = n := by
--   rw [add_zero]
--   rw [add_zero]
