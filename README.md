# Advanced Formal Tools 2: Project 4 

Members:

- WEN Jing
- NGUYEN Stéphane Liem

In this project, we should study the feasability of using an AI to formalise the logic. We know that AI does not handle semantics well. The ambiguity of human speech also makes it difficult to formalise.

- However, instead of using it as a fully automated tool, could we use it as an assistant tool? This work should use *prompt engineering* to guide AI to give the good answer, using the correct syntax.

- How this tool can be useful ?
- What are the limits ?
- Can we infer some trivial parts of a proof ?

To this aim, you *may* look at semantics of programming language. Try to formalise an elementary programming language and one of its derivations.

---

Resources:
- [ChatGPT](https://openai.com/blog/chatgpt/): ChatGPT
- [New York Times](https://www.nytimes.com/2023/07/02/science/ai-mathematics-machine-learning.html): An article about this new trend
- [LeanDojo](https://www.marktechpost.com/2023/07/01/can-llms-generate-mathematical-proofs-that-can-be-rigorously-checked-meet-leandojo-an-open-source-ai-playground-with-toolkits-benchmarks-and-models-for-large-language-models-to-prove-formal-theore/): An AI proof assistant
- [List of projects](https://gitlab.unige.ch/smv/outils-formels-avanc-s/ressources-2024)

